/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tests;
import questions.Question;
import questions.QuestionBank;
import questions.TrueFalseQuestion;
import courses.Course;
import java.io.*;
import java.util.*;
/**
 *
 *
 * @author etranchill
 */
public class Driver
{
    public static void main(String args[])
    {
        List<String> tagList = new ArrayList<String>();
        Course course = new Course(null, 5);
        
        TrueFalseQuestion Q1 = new TrueFalseQuestion("Question 1", null, tagList, 10, "blahh", course, true);
        TrueFalseQuestion Q2 = new TrueFalseQuestion("Question 2", null, tagList, 5, "wtf", course, false);
        TrueFalseQuestion Q3 = new TrueFalseQuestion("Question 3", null, tagList, 8, "nooo", course, true);
        TrueFalseQuestion Q4 = new TrueFalseQuestion("Question 4", null, tagList, 1, "blahh", course, false);       
        
        ArrayList<Question> questionList = new ArrayList<Question>();
        questionList.add(Q1);
        questionList.add(Q2);
        questionList.add(Q3);
        questionList.add(Q4);
        
        QuestionBank QB1 = new QuestionBank();
        QB1.addQuestion((Question)Q1);
        QB1.addQuestion(Q2);
        QB1.addQuestion(Q3);
        QB1.addQuestion(Q4);
        
        AnswerSheet AS1 = new AnswerSheet();
        
        
        //TestOptions T1 = new TestOptions(questionList, null, null);
        //AS1.setTestOptions(T1);
        
        
        Scanner scan = new Scanner(System.in);
        Answer A1 = new Answer();
       
        System.out.println("Answer 1: ");
        A1.setAnswerBody(scan.nextLine());
        
        AS1.submittedResponses.add(A1);
        
        //AS1.autogradeQuestions();
        
    }
    
}
