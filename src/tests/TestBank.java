package tests;

import java.io.*;
import java.util.*;

/**
 * Created by Brandon on 10/20/2015.
 * Edited by Daniel on 11/28/2015.
 */
public class TestBank implements Serializable {
    /**
     * List of Test Options. New Entry added every time a test is given.
     * Used to help search for tests for student.
     *
     */
    public List<TestOptions>  givenTests;
    
    /**
     * A list of all tests that are available to be taken in TestTool
     */
    public List<Test> testList;
    
    /**
     * Stores the next testId to be assigned
     */
    public static int testId;
    
    /**
     * Constructor for TestBank, initializes testId to zero, creates
     * an empty ArrayList of Tests
     */
    public TestBank() {
        testList = new ArrayList();
        testId = 0;
        givenTests = new ArrayList();
    }

    /**
     * This method adds a new test to the TestTool TestBank
     * @param test a Test object representing the new test to be added
     */
    /*
       pre: !exists(Test otherTest;
          testList.contains(otherTest);
          test.course.equals(otherTest.course) &&
             test.instructor.equals(otherTest.instructor) &&
             test.questionList.size == otherTest.questionList.size &&
             forall (Question currentQuestion;
                     test.questionList.contains(currentQuestion);
                     otherTest.questionList.contains(currentQuestion)) &&
             forall (Question otherQuestion;
                     otherTest.questionList.contains(otherQuestion);
                     test.questionList.contains(otherQuestion)))
      
       post: testList'.size() == testList.size() + 1
             && forall (Test containedTest;
                        testList.contains(containedTest);
                        testList'.contains(containedTest))
             && testList'.contains(test)
      */
    public void addTest(Test test) {
        // add testId to test being added, then increment
        testId++;
        testList.add(test);
    }

    /**
     * This method removes the given test from the TestTool TestBank
     * @param test a Test object representing the test to be removed
     */

    /*
       pre: testList.size() >= 1
            && testList.contains(test)
      
       post: testList'.size == testList.size() - 1
             && !testList'.contains(test)
             && forall (Test containedTest;
                        testList.contains(containedTest) && containedTest != test;
                        testList'.contains(containedTest))
     */
    public void removeTest(Test test) {
        testList.remove(test);
    }

    /**
     * @return the testList
     */
    public List<Test> getTestList() {
        return testList;
    }

    /**
     * @param testList the testList to set
     */
    public void setTestList(List<Test> testList) {
        this.testList = testList;
    }

    /**
     * @return the testId
     */
    public int getTestId() {
        return testId;
    }

    /**
     * @param testId the testId to set
     */
    public void setTestId(int testId) {
        this.testId = testId;
    }
    
    /**
     * Get Given Tests.
     * @return the Given Tests List.
     */
    public List<TestOptions> getGivenTests() {
        return givenTests;
    }
    
    /**
     * 
     * @param list new list of Test Options
     */
    public void setGivenTests(List<TestOptions> list) {
        this.givenTests = list;
    }
}
