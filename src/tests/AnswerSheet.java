package tests;

import java.io.*;
import users.Student;

import java.util.*;
import questions.CodeQuestion;
import questions.EssayQuestion;
import questions.FillInQuestion;
import questions.MCQuestion;
import questions.MatchingQuestion;
import questions.Question;
import questions.TrueFalseQuestion;

/**
 * Created by Brandon on 10/20/2015.
 */
public class AnswerSheet implements Serializable {
    /**
     * The student that took the test
     */
    public Student student;

    /**
     * The amount of time taken for the given test store in minutes
     */
    public int minutesTaken;

    /**
     * The date in which the given test was taken
     */
    public Date date;

    /**
     * Flag determining if the test has been graded
     */
    public boolean isGraded;

    /**
     * The test that the given answer sheet refers to
     */
    public TestOptions testOptions;

    /**
     * The list of test answers submitted by the student
     */
    public List<Answer> submittedResponses;

    /**
     * The overall score for the test stored as a fraction
     */
    public double overallScore;

    /**
     * Preliminarily grades all possible questions in the test
     */
    public void autogradeQuestions() {
        double currentScore = 0.0;
        double currentPossible = 0.0;
        for (int i = 0; i < testOptions.getQuestionList().size(); i++) {
            Answer currentAnswer = submittedResponses.get(i);
            Question currentQuestion = testOptions.getQuestionList().get(i);
            if (currentQuestion.getClass() == CodeQuestion.class) {
                currentQuestion.gradeQuestion(null);
            } else if (currentQuestion.getClass() == EssayQuestion.class) {
                Scanner scanner = new Scanner(currentAnswer.answerBody);
                List<String> answerList = new ArrayList();
                while (scanner.hasNext()) {
                    answerList.add(scanner.next());
                }
                currentAnswer.score = currentQuestion.gradeQuestion(answerList);
            } else if (currentQuestion.getClass() == FillInQuestion.class) {
                currentAnswer.score = currentQuestion.gradeQuestion(currentAnswer.answerBody);
            } else if (currentQuestion.getClass() == MCQuestion.class) {
                Scanner scanner = new Scanner(currentAnswer.answerBody);
                Map<String, Boolean> answerMap = new HashMap();
                for (String key: ((MCQuestion) currentQuestion).answers.keySet()) {
                    answerMap.put(key, Boolean.valueOf(scanner.next()));
                }
                currentAnswer.score = currentQuestion.gradeQuestion(answerMap);
            } else if (currentQuestion.getClass() == MatchingQuestion.class) {
                Scanner scanner = new Scanner(currentAnswer.answerBody);
                Map<String, String> answerMap = new HashMap();
                for (String key: ((MatchingQuestion) currentQuestion).answerKey.keySet()) {
                    answerMap.put(key, scanner.next());
                }
                currentAnswer.score = currentQuestion.gradeQuestion(answerMap);
            } else if (currentQuestion.getClass() == TrueFalseQuestion.class) {
                if (currentAnswer.answerBody.toLowerCase().equals("true")) {
                    currentAnswer.score = currentQuestion.gradeQuestion(true);
                } else {
                    currentAnswer.score = currentQuestion.gradeQuestion(false);
                }
            } else {
                throw new UnsupportedOperationException();
            }
            currentScore += currentAnswer.score * currentAnswer.pointValue;
            currentPossible += currentAnswer.score;
        }
        overallScore = currentScore / currentPossible;
        isGraded = true;
    }

    /**
     * This method computes the overall score based on the individual scores for each question
     */

    /*
       Pre: submmitedResponses.size() >= 1
      
       Post: overallScore != null 
             && overallScore >= 0
     */
    public void calculateOverallScore() { //May not need this
        System.out.println("AnswerSheet.calculateOverallScore");
    }

    /**
     * @return the student
     */
    public Student getStudent() {
        return student;
    }

    /**
     * @param student the student to set
     */
    public void setStudent(Student student) {
        this.student = student;
    }

    /**
     * @return the minutesTaken
     */
    public int getMinutesTaken() {
        return minutesTaken;
    }

    /**
     * @param minutesTaken the minutesTaken to set
     */
    public void setMinutesTaken(int minutesTaken) {
        this.minutesTaken = minutesTaken;
    }

    /**
     * @return the date
     */
    public Date getDate() {
        return date;
    }

    /**
     * @param date the date to set
     */
    public void setDate(Date date) {
        this.date = date;
    }

    /**
     * @return the isGraded
     */
    public boolean isIsGraded() {
        return isGraded;
    }

    /**
     * @param isGraded the isGraded to set
     */
    public void setIsGraded(boolean isGraded) {
        this.isGraded = isGraded;
    }

    /**
     * @return the test
     */
    public Test getTestOptions() {
        return testOptions;
    }

    /**
     * @param test the test to set
     */
    public void setTest(TestOptions test) {
        this.testOptions = test;
    }

    /**
     * @return the submittedResponses
     */
    public List<Answer> getSubmittedResponses() {
        return submittedResponses;
    }

    /**
     * @param submittedResponses the submittedResponses to set
     */
    public void setSubmittedResponses(List<Answer> submittedResponses) {
        this.submittedResponses = submittedResponses;
    }

    /**
     * @return the overallScore
     */
    public double getOverallScore() {
        return overallScore;
    }

    /**
     * @param overallScore the overallScore to set
     */
    public void setOverallScore(double overallScore) {
        this.overallScore = overallScore;
    }
}
