package tests;

import courses.Course;
import courses.SectionData;
import gui.Controller;
import java.io.*;
import questions.Question;
import users.Instructor;
import java.util.*;
import java.util.InputMismatchException;

/**
 * Created by Brandon on 10/19/2015.
 * Edited by Daniel on 11/30/2015.
 */
public class Test implements Serializable {
    /**
     * A Name for the given test;
     */
    public String testName;
    
    /**
     * A unique identifier for the given test
     */
    public int testId;
    
    /**
     * List of all questions associated with the given test
     */
    public List<Question> questionList;

    /**
     * The instructor that created the given test
     */
    public Instructor creator;

    /**
     * The course that the given test was written for
     */
    public Course course;
    
    /**
     * Boolean value determining if the test is currently active
     */
    public boolean isActive;
    
    /**
     * List of Answer Sheets for a Test
     */
    public List<AnswerSheet> answerSheets;
    
    /**
     * Constructor for Test, creates a new Test given a List of Questions,
     * Instructor, and Course
     * @param questionList list of questions on the test
     * @param creator Instructor that created the test
     * @param course Course that this is related to
     */
    public Test(String testName, List<Question> questionList, Instructor creator, Course course) {
        if (questionList == null || questionList.size() < 1
                || creator == null || course == null) {
            throw new InputMismatchException();
        }
        this.testName = testName;
        this.questionList = questionList;
        this.creator = creator;
        this.course = course;
        answerSheets = new ArrayList<AnswerSheet>();
        
    }
    
    /**
     * Constructor for Test, creates a new Test NOT given a List of Questions,
     * Instructor, and Course
     * @param questionList list of questions on the test
     * @param creator Instructor that created the test
     * @param course Course that this is related to
     */
    public Test(Instructor creator, Course course) {
        if (creator == null || course == null) {
            throw new InputMismatchException();
        }

        this.creator = creator;
        this.course = course;
    }
    
    public Test(Instructor creator) {
        if (creator == null) {
            throw new InputMismatchException();
        }

        this.creator = creator;
        this.questionList = new ArrayList();
    }
    
    

    /**
     * This method adds the given test to the lists of available tests for 
     * students enrolled in the provided course
     * @param courseSection the course intended to take the given test
     * @param options test options 
     */

     /*
        pre: exist(courseSection)
            && course.contains(courseSection)
      
        post: testList'.size() == testList.size() + 1
     */
    public void giveTest(SectionData courseSection, TestOptions options) {
        System.out.println("Test.giveTest");
        Controller.giveTest(courseSection, options);
    }
    
    /**
     * Given a list of question id's, if adds each question to the Test.
     * @param questionIds a list of question id's
    */
    public void addQuestions(List<Integer> questionIds) {
        for (Integer id: questionIds) {
            this.addQuestion(id);
        }
    }

    /**
     * This method adds the given question to the Test's questionList
     * @param questionId the Id of the question to be added
     */
    public void addQuestion(int questionId) {
        System.out.println("Test.addQuestion");
        boolean found = false;
        for (Question question: Controller.qBank.questionList) {
            if (question.questionId == questionId) {
                getQuestionList().add(question);
                found = true;
                break;
            }
        }
        if (!found) {
            throw new InputMismatchException();
        }
    }

    /**
     * This method removes the given question from the Test's questionList
     * @param questionId the question to be added
     */
    public void removeQuestion(int questionId) {
        System.out.println("Test.removeQuestion");
        boolean found = false;
        
        for (Question question: Controller.qBank.questionList) {
            if (question.questionId == questionId) {
                getQuestionList().remove(question);
                found = true;
                break;
            }
        }
        if (!found)
            throw new InputMismatchException();
    }

    /**
     * @return the questionList
     */
    public List<Question> getQuestionList() {
        return questionList;
    }

    /**
     * @param questionList the questionList to set
     */
    public void setQuestionList(List<Question> questionList) {
        this.questionList = questionList;
    }

    /**
     * @return the creator
     */
    public Instructor getCreator() {
        return creator;
    }

    /**
     * @param creator the creator to set
     */
    public void setCreator(Instructor creator) {
        this.creator = creator;
    }

    /**
     * @return the course
     */
    public Course getCourse() {
        return course;
    }

    /**
     * @param course the course to set
     */
    public void setCourse(Course course) {
        this.course = course;
    }

    /**
     * @return the testId
     */
    public int getTestId() {
        return testId;
    }

    /**
     * @param testId the testId to set
     */
    public void setTestId(int testId) {
        this.testId = testId;
    }

    /**
     * @return the isActive
     */
    public boolean isIsActive() {
        return isActive;
    }

    /**
     * @param isActive the isActive to set
     */
    public void setActive(boolean isActive) {
        this.isActive = isActive;
    }
    
    /**
     * @return Returns the Test Name
     */
    public String getTestName() {
        return testName;
    }
    
    /**
     * @param testName Test Name to be changed
     */
    public void setTestName(String testName) {
        this.testName = testName;
    }
    
    
    /**
     * Adds Answer Sheets to Test (When Student Submits)
     * @param answerSheet answer sheet to be added
     */
    public void addAnswerSheet(AnswerSheet answerSheet) {
        answerSheets.add(answerSheet);
    }
    
    /**
     *Get Answer Sheet List 
     */
    public List<AnswerSheet> getAnswerSheets() {
        return answerSheets;
    }
    
    /**
     * Set Entire List of AnswerSheets
     */
    public void setAnswerSheets(List<AnswerSheet> answerSheets) {
            this.answerSheets = answerSheets;
    }
    
    public AnswerSheet findAnswerSheet(String studentID) {
        AnswerSheet as = null;
        for(int i = 0; i < answerSheets.size(); i++) {
            String id = answerSheets.get(i).getStudent().getCalPolyId();
            if(id.equals(studentID)){
                as = answerSheets.get(i);
            }
        }
        
        return as;
    }
}   
