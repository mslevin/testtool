package tests;

import courses.Course;
import courses.SectionData;
import java.io.*;

import java.util.*;
import questions.Question;
import users.Instructor;

/**
 * Created by Brandon on 10/20/2015.
 */
public class TestOptions extends Test implements Serializable {
    
    // Need to add a lst of answersheets to this!!!!!!
    
    /**
     * The opening time for the given test
     */
    public Date openTime;

    /**
     * The deadline for the given test
     */
    public Date closeTime;

    /**
     * The time limit in minutes for the given test
     */
    public int timeLimit;

    /**
     * The section intended to take the given test
     */
    public SectionData section;

    /**
     * Determines if the student is allowed to go back and review their
     * answers after grading
     */
    public boolean canReview;
    
    public TestOptions(String testName, List<Question> questionList, Instructor creator,
            Course course, Date openTime, Date closeTime, int timeLimit,
            SectionData section, boolean canReview) {
        super(testName, questionList, creator, course);
        if (openTime == null || closeTime == null || timeLimit < 1 ||
                section == null) {
            throw new InputMismatchException();
        }
        this.openTime = openTime;
        this.closeTime = closeTime;
        this.timeLimit = timeLimit;
        this.section = section;
        this.canReview = canReview;
    }

    /**
     * Sets the openTime to the current time
     */
    public void releaseTest() {
        isActive = true;
    }

    /**
     * Extends the closeTime of the test to the given time
     * @param newDeadline the new test deadline
     */
    public void extendTest(Date newDeadline) {
        closeTime = newDeadline;
    }

    /**
     * @return the openTime
     */
    public Date getOpenTime() {
        return openTime;
    }

    /**
     * @param openTime the openTime to set
     */
    public void setOpenTime(Date openTime) {
        this.openTime = openTime;
    }

    /**
     * @return the closeTime
     */
    public Date getCloseTime() {
        return closeTime;
    }

    /**
     * @param closeTime the closeTime to set
     */
    public void setCloseTime(Date closeTime) {
        this.closeTime = closeTime;
    }

    /**
     * @return the timeLimit
     */
    public int getTimeLimit() {
        return timeLimit;
    }

    /**
     * @param timeLimit the timeLimit to set
     */
    public void setTimeLimit(int timeLimit) {
        this.timeLimit = timeLimit;
    }

    /**
     * @return the section
     */
    public SectionData getSection() {
        return section;
    }

    /**
     * @param section the section to set
     */
    public void setSection(SectionData section) {
        this.section = section;
    }

    /**
     * @return the canReview
     */
    public boolean isCanReview() {
        return canReview;
    }

    /**
     * @param canReview the canReview to set
     */
    public void setCanReview(boolean canReview) {
        this.canReview = canReview;
    }
}
