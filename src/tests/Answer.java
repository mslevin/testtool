package tests;

import java.io.*;

/**
 * Created by Brandon on 10/20/2015.
 */
public class Answer implements Serializable {
    /**
     * The text of the answer
     */
    public String answerBody;

    /**
     * The point value for the given question
     */
    public int pointValue;

    /**
     * The score for the given question stored as a fraction
     */
    public double score;

    /**
     * A string containing grader comments for the given question
     */
    public String graderComments;

    /**
     * Overrides an existing score with the new provided score
     * @param newScore the new score to be set as the grade for the given question
     */

    /*
       Pre: score >= 0 
            && newScore >= 0
            && pointValue >= 0
      
       Post: score >= 0 
            && pointValue >= 0
     */
    public void overrideScore(double newScore) {
        score = newScore;
    }

    /**
     * @return the answerBody
     */
    public String getAnswerBody() {
        return answerBody;
    }

    /**
     * @param answerBody the answerBody to set
     */
    public void setAnswerBody(String answerBody) {
        this.answerBody = answerBody;
    }

    /**
     * @return the pointValue
     */
    public int getPointValue() {
        return pointValue;
    }

    /**
     * @param pointValue the pointValue to set
     */
    public void setPointValue(int pointValue) {
        this.pointValue = pointValue;
    }

    /**
     * @return the score
     */
    public double getScore() {
        return score;
    }

    /**
     * @param score the score to set
     */
    public void setScore(double score) {
        this.score = score;
    }

    /**
     * @return the graderComments
     */
    public String getGraderComments() {
        return graderComments;
    }

    /**
     * @param graderComments the graderComments to set
     */
    public void setGraderComments(String graderComments) {
        this.graderComments = graderComments;
    }
}
