package questions;

import courses.Course;
import java.awt.Image;
import java.io.Serializable;
import java.util.*;
/**
 * Created by Brandon on 10/19/2015.
 */
public class EssayQuestion extends Question implements Serializable {
    public EssayQuestion(String questionBody, Image image, List<String> tagList,
            int estimatedDifficulty, String additionalText, Course course, String keyWords) {
        super(questionBody, image, tagList, estimatedDifficulty, additionalText, course);
        
        answerKeywords = new ArrayList<String>(Arrays.asList(keyWords.split(",")));
    }
    
    /**
     * A list of keywords to preliminarily grade a question
     */
    public List<String> answerKeywords;

    /**
     * @return the answerKeywords
     */
    public List<String> getAnswerKeywords() {
        return answerKeywords;
    }

    /**
     * @param answerKeywords the answerKeywords to set
     */
    public void setAnswerKeywords(List<String> answerKeywords) {
        this.answerKeywords = answerKeywords;
    }
    
    @Override
    public double gradeQuestion(Object o) {
        //o should be of type ArrayList<String>
        if (o.getClass() != ArrayList.class) {
            throw new UnsupportedOperationException();
        }
        double listSize = answerKeywords.size();
        double correctMatches = 0;
        for (String s: (ArrayList<String>) o) {
            for (String keyword: answerKeywords) {
                if (s.equals(keyword)) {
                    correctMatches++;
                    break;
                }
            }
        }
        return correctMatches / listSize;
    }
}
