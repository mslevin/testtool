package questions;

import courses.Course;
import java.awt.Image;
import java.io.Serializable;
import java.util.*;
/**
 * Created by Brandon on 10/19/2015.
 */
public class MCQuestion extends Question implements Serializable{
    public MCQuestion(String questionBody, Image image, List<String> tagList,
            int estimatedDifficulty, String additionalText, Course course, Map<String, Boolean> answers) {
        super(questionBody, image, tagList, estimatedDifficulty, additionalText, course);
        
        this.answers = answers;
        
    }
    
    /**
     * Contains a map for each possible answer holding a boolean value
     * if the chosen answer should have been selected
     */
    public Map<String, Boolean> answers;

    /**
     * @return the answers
     */
    public Map<String, Boolean> getAnswers() {
        return answers;
    }

    /**
     * @param answers the answers to set
     */
    public void setAnswers(Map<String, Boolean> answers) {
        this.answers = answers;
    }
    
    @Override
    public double gradeQuestion(Object o) {
        //o should be of type Map<String, Boolean>
        if (o.getClass() != Map.class) {
            throw new UnsupportedOperationException();
        }
        double numCorrect = 0.0;
        double numWrong = 0.0;
        double numPossible = 0.0;
        
        for (String s: answers.keySet()) {
            if (((Map<String, Boolean>) o).get(s).equals(answers.get(s))) {
                numCorrect++;
            } else {
                numWrong++;
            }
            if (answers.get(s)) {
                numPossible++;
            }
        }
        if ((numCorrect - numWrong)/ numPossible > 0) {
            return (numCorrect - numWrong)/ numPossible;
        }
        return 0.0;
    }
}
