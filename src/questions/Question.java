package questions;

import courses.Course;
import java.awt.*;
import java.util.*;
import java.util.List;
import java.io.*;

/**
 * Created by Brandon on 10/19/2015.
 */
public class Question implements Serializable {
    /**
     * The unique identifier for a given question
     */
    public int questionId;
    
    /**
     * The text containing the main question body
     */
    public String questionBody;

    /**
     * The image associated with the given question
     */
    public Image image;

    /**
     * A list of all tags categorizing the given question
     */
    public List<String> tagList;

    /**
     * An integer representation in minutes of the estimated time to be allotted for the given question
     */
    public int estimatedDifficulty;

    /**
     * Additional text to be supplied with the main question body
     */
    public String additionalText;

    /**
     * The course that the question is intended for
     */
    public Course course;
    
    /**
     * Constructor for Question object. Checks for valid input,
     * construct if valid, throws InputMismatchException if invalid
     * @param questionBody
     * @param image
     * @param tagList
     * @param estimatedDifficulty
     * @param additionalText
     * @param course
     */
    public Question(String questionBody, Image image, List<String> tagList,
            int estimatedDifficulty, String additionalText, Course course) {
        if (questionBody == null || course == null
                || estimatedDifficulty <= 0) {
            throw new InputMismatchException();
        }
        
        this.questionId = -1;
        this.questionBody = questionBody;
        this.image = image;
        this.tagList = tagList;
        this.estimatedDifficulty = estimatedDifficulty;
        this.additionalText = additionalText;
        this.course = course;
    }
    
    
    /**
     * Set the questionId of the current question
     * @param questionId 
     */
    public void setId(int questionId) {
        this.questionId = questionId;
    }
    
    /**
     * @return the questionId of the question
     */
    public int getId() {
        return this.questionId;
    }
    
    /**
     * @return the questionBody
     */
    public String getQuestionBody() {
        return questionBody;
    }

    /**
     * @param questionBody the questionBody to set
     */
    public void setQuestionBody(String questionBody) {
        this.questionBody = questionBody;
    }

    /**
     * @return the image
     */
    public Image getImage() {
        return image;
    }

    /**
     * @param image the image to set
     */
    public void setImage(Image image) {
        this.image = image;
    }

    /**
     * @return the tagList
     */
    public List<String> getTagList() {
        return tagList;
    }

    /**
     * @param tagList the tagList to set
     */
    public void setTagList(List<String> tagList) {
        this.tagList = tagList;
    }

    /**
     * @return the estimatedDifficulty
     */
    public int getEstimatedDifficulty() {
        return estimatedDifficulty;
    }

    /**
     * @param estimatedDifficulty the estimatedDifficulty to set
     */
    public void setEstimatedDifficulty(int estimatedDifficulty) {
        this.estimatedDifficulty = estimatedDifficulty;
    }

    /**
     * @return the additionalText
     */
    public String getAdditionalText() {
        return additionalText;
    }

    /**
     * @param additionalText the additionalText to set
     */
    public void setAdditionalText(String additionalText) {
        this.additionalText = additionalText;
    }

    /**
     * @return the course
     */
    public Course getCourse() {
        return course;
    }

    /**
     * @param course the course to set
     */
    public void setCourse(Course course) {
        this.course = course;
    }

    /**
     * @return the questionId
     */
    public int getQuestionId() {
        return questionId;
    }

    /**
     * @param questionId the questionId to set
     */
    public void setQuestionId(int questionId) {
        this.questionId = questionId;
    }
    
    public double gradeQuestion(Object o) {
       throw new UnsupportedOperationException();
    }
}
