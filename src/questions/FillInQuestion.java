package questions;

import courses.Course;
import java.awt.Image;
import java.io.*;
import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.List;
import java.util.Scanner;

/**
 * Created by Brandon on 10/20/2015.
 */
public class FillInQuestion extends Question implements Serializable {
    public FillInQuestion(String questionBody, Image image, List<String> tagList,
            int estimatedDifficulty, String additionalText, Course course, String fillInAnswer) {
        super(questionBody, image, tagList, estimatedDifficulty, additionalText, course);
        
        answer = fillInAnswer;
        
    }
    
    /**
     * The string that successfully fills in the blank to complete the question
     */
    public String answer;

    /**
     * @return the answer
     */
    public String getAnswer() {
        return answer;
    }

    /**
     * @param answer the answer to set
     */
    public void setAnswer(String answer) {
        this.answer = answer;
    }
    
    @Override
    public double gradeQuestion(Object o) {
        //o should be of type String
        if (o.getClass() != String.class) {
            throw new UnsupportedOperationException();
        }
        if (answer.equals((String) o)) {
            return 1.0;
        } else {
            return 0;
        }
    }
}
