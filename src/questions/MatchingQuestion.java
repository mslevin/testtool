package questions;

import courses.Course;
import java.awt.Image;
import java.io.Serializable;
import java.util.*;
/**
 * Created by Brandon on 10/20/2015.
 */
public class MatchingQuestion extends Question implements Serializable {
    public MatchingQuestion(String questionBody, Image image, List<String> tagList,
            int estimatedDifficulty, String additionalText, Course course,
            Map<String, String> answerKey) {
        super(questionBody, image, tagList, estimatedDifficulty, additionalText, course);
        
        this.answerKey = answerKey;
        
    }
    
    /**
     * Stores a map of the correct pairing of answers
     */
    public Map<String, String> answerKey;

    /**
     * @return the answerKey
     */
    public Map<String, String> getAnswerKey() {
        return answerKey;
    }

    /**
     * @param answerKey the answerKey to set
     */
    public void setAnswerKey(Map<String, String> answerKey) {
        this.answerKey = answerKey;
    }
    
    @Override
    public double gradeQuestion(Object o) {
        //o should be of type Map<String, String>
        if (o.getClass() != Map.class) {
            throw new UnsupportedOperationException();
        }
        double numCorrect = 0.0;
        
        for (String s: answerKey.keySet()) {
            if (((Map<String, String>) o).get(s).equals(answerKey.get(s))) {
                numCorrect++;
            }
        }
        return numCorrect / answerKey.keySet().size();
    }
}
