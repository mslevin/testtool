package questions;

import courses.Course;
import java.awt.Image;
import java.io.Serializable;
import java.util.*;

/**
 * Created by Brandon on 10/19/2015.
 */
public class TrueFalseQuestion extends Question implements Serializable {
    public TrueFalseQuestion(String questionBody, Image image, List<String> tagList,
            int estimatedDifficulty, String additionalText, Course course, boolean answer) {
        super(questionBody, image, tagList, estimatedDifficulty, additionalText, course);
        
        this.answer = answer;
    }
    
    /**
     * The answer to the true/false question
     */
    public boolean answer;

    /**
     * @return the answer
     */
    public boolean isAnswer() {
        return answer;
    }

    /**
     * @param answer the answer to set
     */
    public void setAnswer(boolean answer) {
        this.answer = answer;
    }
    
    @Override
    public double gradeQuestion(Object o) {
        //o should be of type Boolean
        if (o.getClass() != Boolean.class) {
            throw new UnsupportedOperationException();
        }
        if (((Boolean) o).equals(answer)) {
            return 1.0;
        }
        return 0.0;
    }
}
