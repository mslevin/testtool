package questions;

import courses.Course;
import java.awt.Image;
import java.io.Serializable;
import java.util.List;
/**
 * Created by Brandon on 10/19/2015.
 */
public class CodeQuestion extends Question implements Serializable {
    public CodeQuestion(String questionBody, Image image, List<String> tagList,
            int estimatedDifficulty, String additionalText, Course course) {
        super(questionBody, image, tagList, estimatedDifficulty, additionalText, course);
    }
    
    @Override
    public double gradeQuestion(Object o) {
        //no preliminary grading done
        return 0.0;
    }
}
