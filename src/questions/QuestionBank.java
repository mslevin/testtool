package questions;

import java.util.Collection;
import java.util.List;
import java.io.Serializable;
import java.util.ArrayList;
/**
 * Created by Brandon on 10/19/2015.
 */
public class QuestionBank implements Serializable{
    /**
     * A list of all saved questions within TestTool
     */
    public List<Question> questionList;
    
    /**
     * Stores the next questionId to be assigned
     */
    public static int questionId;
    
    public QuestionBank() {
        questionList = new ArrayList();
        questionId = 0;
    }

    /**
     * This method adds a new question to the TestTool QuestionBank
     * @param question a question object representing the new question to be added
     */
    /*
       pre: !exists(Question otherQuestion;
          questionList.contains(otherQuestion);
          question.questionBody.equals(otherQuestion.questionBody)
          && question.image.equals(otherQuestion.image)
          && question.estimatedDifficulty.equals(otherQuestion.estimatedDifficulty)
          && question.additionalText.equals(otherQuestion.additionalTest)
          && question.course.equals(otherQuestion.course)
          && forall (String currentTag;
                        question.tagList.contains(currentTag);
                        otherQuestion.tagList.contains(currentTag))
          && forall (String otherTag;
                        otherQuestion.tagList.contains(otherTag);
                        question.tagList.contains(otherTag)))
      
       post: questionList'.size() == questionList.size() + 1
             && forall (Question containedQuestion;
                        questionList.contains(containedQuestion);
                        questionList'.contains(containedQuestion))
             && questionList'.contains(question)
     */
    public void addQuestion(Question question) {
        question.setQuestionId(questionId++);
        questionList.add(question);
    }

    /**
     * This method removes a question from the TestTool QuestionBank
     * @param questionId a questionId representing the new question to be removed
     * @throws java.lang.Exception
     */
    /*
       pre: questionList.size() >= 1
            && questionList.contains(question)
      
       post: questionList'.size == questionList.size() - 1
             && !questionList'.contains(question)
             && forall (Question containedQuestion;
                        questionList.contains(containedQuestion) && containedQuestion != question;
                        questionList'.contains(containedQuestion))
      */
    public void removeQuestion(int questionId) throws Exception {
        int index = -1;
        
        for (Question q : questionList) {
            if ((index = q.getQuestionId()) == questionId)
                break;
            
            throw new Exception("Cannot find question " + index + " to remove.");
        }
        
        questionList.remove(index);
    }
    
    /**
     * Edit the question corresponding to the given questionId
     * @param questionId
     * @throws Exception
     */
    public void editQuestion(int questionId) throws Exception {
        Question question = null;
        
        for (Question q : questionList) {
            if (q.getQuestionId() == questionId) {
                question = q;
                break;
            }
            throw new Exception("Cannot find question " + questionId + " to edit.");
        }
        
        addQuestion(question);
    }
    
    public Collection getAllQuestions() {
        return getQuestionList();
    }

    /**
     * @return the questionList
     */
    public List<Question> getQuestionList() {
        return questionList;
    }

    /**
     * @param questionList the questionList to set
     */
    public void setQuestionList(List<Question> questionList) {
        this.questionList = questionList;
    }
}
