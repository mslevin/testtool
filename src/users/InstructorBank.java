package users;

import java.util.*;
import java.io.*;
/**
 * Created by Brandon on 10/20/2015.
 */
public class InstructorBank implements Serializable {
    /**
     * A list of all instructors that are registered to use TestTool
     */
    public List<Instructor> instructorList;

    public InstructorBank() {
        this.instructorList = new ArrayList();
    }

    /**
     * This method adds a new instructor to the TestTool InstructorBank
     * @param instructor an instructor object representing the new instructor to be added
     */
    /*
       pre: !exists(Instructor otherInstructor;
          instructorList.contains(otherInstructor);
          instructor.department.equals(otherInstructor.department)
          && forall (SectionData currentCourse;
                        instructor.classList.contains(currentCourse);
                        otherInstructor.classList.contains(currentCourse))
          && forall (SectionData otherCourse;
                        otherInstructor.classList.contains(otherCourse);
                        instructor.classList.contains(otherCourse)))
      
       post: instructorList'.size() == instructorList.size() + 1
             && forall (Instructor containedInstructor;
                        instructorList.contains(containedInstructor);
                        instructorList'.contains(containedInstructor))
             && instructorList'.contains(instructor)
     */
    public void addInstructor(Instructor instructor) {
        // This needs to check whether or not this instructor actually exists or not
        // Edit it to return true/false based off a successful addition
        // Same goes for all the banks.
        instructorList.add(instructor);
    }

    /**
     * This method removes the given instructor from the TestTool InstructorBank
     * @param instructor an instructor object representing the instructor to be removed
     */
    /*
       pre: instructorList.size() >= 1
            && instructorList.contains(instructor)
      
       post: instructorList'.size == instructorList.size() - 1
             && !instructorList'.contains(instructor)
             && forall (Instructor containedInstructor;
                        courseList.contains(containedInstructor) && containedInstructor != instructor;
                        courseList'.contains(containedInstructor))
     */
    public void removeInstructor(Instructor instructor) {
        // Needs to actually check that this instructor exists
        instructorList.remove(instructor);
    }

    /**
     * @return the instructorList
     */
    public List<Instructor> getInstructorList() {
        return instructorList;
    }

    /**
     * @param instructorList the instructorList to set
     */
    public void setInstructorList(List<Instructor> instructorList) {
        this.instructorList = instructorList;
    }
    
    /**
     * @param username the id of the instructor to be returned
     * @return instructor object that matches the given id
     */
    public Instructor getInstructorById(String id) {
        for(Instructor i : instructorList) {
            if (i.calPolyId.equals(id)) {
                return i;
            }
        }
        return null;
    }
}
