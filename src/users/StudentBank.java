package users;

import gui.Controller;
import java.io.*;
import java.util.*;

/**
 * Created by Brandon on 10/20/2015.
 */
public class StudentBank implements Serializable {
    /**
     * A list of all students that are registered to use TestTool
     */
    public List<Student> studentList;
    
    public StudentBank() {
        studentList = new ArrayList();
    }

    /**
     * This method adds a new student to the TestTool StudentBank
     * @param student a student object representing the new student to be added
     */
    /*
       pre: !exists(Student otherStudent;
          studentList.contains(otherStudent);
          student.major.equals(otherStudent.major)
          && forall (SectionData currentCourse;
                        student.classList.contains(currentCourse);
                        otherStudent.classList.contains(currentCourse))
          && forall (SectionData otherCourse;
                        otherStudent.classList.contains(otherCourse);
                        student.classList.contains(otherCourse)))
      
       post: studentList'.size() == studentList.size() + 1
             && forall (Student containedStudent;
                        studentList.contains(containedStudent);
                        studentList'.contains(containedStudent))
             && studentList'.contains(student)
     */
    public void addStudent(Student student) {
        if (studentList == null) {
            System.out.println("studentList is null");
            return;
        }
        studentList.add(student);
    }

    /**
     * This method removes the given student from the TestTool StudentBank
     * if it is also provided valid instructor credentials
     * @param student a student object representing the student to be removed
     */
    /*
       pre: studentList.size() >= 1
            && studentList.contains(student)
      
       post: studentList'.size == studentList.size() - 1
             && !studentList'.contains(student)
             && forall (Student containedStudent;
                        studentList.contains(containedStudent) && containedStudent != student;
                        studentList'.contains(containedStudent))
     */
    public void removeStudent(Student student) {
        if (Controller.currUser.getClass().getSimpleName().equals("Instructor")) {
            studentList.remove(student);
        }
    }

    /**
     * @return the studentList
     */
    public List<Student> getStudentList() {
        return studentList;
    }

    /**
     * @param studentList the studentList to set
     */
    public void setStudentList(List<Student> studentList) {
        this.studentList = studentList;
    }
}
