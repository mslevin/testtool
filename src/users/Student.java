package users;

import courses.SectionData;
import gui.Controller;
import java.io.*;
import java.util.*;
import tests.AnswerSheet;
import tests.Test;
import tests.TestOptions;

/**
 * Created by Brandon on 10/19/2015.
 * Edited by Tim on 12/3/15
 */
public class Student extends User implements Serializable {
    /**
     * The major of the given student
     */
    public String major;

    /**
     * A list of the given student's courses
     */
    public List<SectionData> courseList;
    
    /**
     * A list of the given students past tests
     */
    public List<AnswerSheet> takenTestList;
    
    public Student(String first, String last, String major, String id, String password) {
        super(first, last, id, password);
        if (major == null) { 
            throw new InputMismatchException();
        }
        this.major = major;
        takenTestList = new ArrayList();
        courseList = new ArrayList();
    }

    /**
     * Adds a new course to the list of courses taken by the given student
     * @param newCourse the new course to be added
     */
    /*
       pre: !exists(Course otherCourse;
          courseList.contains(otherCourse);
          course.department.equals(otherCourse.department)
          && course.courseNumber == otherCourse.courseNumber
      
       post: courseList'.size() == courseList.size() + 1
             && forall (Course containedCourse;
                        courseList.contains(containedCourse);
                        courseList'.contains(containedCourse))
             && courseList'.contains(containedCourse)
     */
    public void addCourse(SectionData newCourse) {
        courseList.add(newCourse);
    }

    /**
     * Calls the getTests function from the Controller.
     * 
     * @param studentID Student's ID to find tests.
     * @return ArrayList of TestOptions of all active and tests in history
     * 
     */
    public ArrayList<TestOptions> getTests(String studentID) {
        ArrayList<TestOptions> tests;
        tests = Controller.getTests(studentID);
        
        return tests;
        
    }
    
    /**
     * Removes a course from the list of courses taken by the given student
     * @param course the course to be removed
     */
    /*
       pre: courseList.size() >= 1
            && courseList.contains(course)
      
       post: courseList'.size == courseList.size() - 1
             && !courseList'.contains(course)
             && forall (Course containedCourse;
                        courseList.contains(containedCourse) && containedCourse != newCourse;
                        courseList'.contains(containedCourse))
     */
    public void removeCourse(SectionData course) {
        courseList.remove(course);
    }

    /**
     * Instantiates the given test to be taken by the student
     * @param test the test to be taken
     */
    public void startTest(Test test) {
        System.out.println("Student.startTest");
        //TODO
    }

    /**
     * Retrieves results of the students attempt at the given test
     * @param test the test to be retrieved
     */
    public void reviewTest(Test test) {
        System.out.println("Student.reviewTest");
        //TODO
    }

    /**
     * @return the major
     */
    public String getMajor() {
        return major;
    }

    /**
     * @param major the major to set
     */
    public void setMajor(String major) {
        this.major = major;
    }

    /**
     * @return the courseList
     */
    public List<SectionData> getCourseList() {
        return courseList;
    }

    /**
     * @param courseList the courseList to set
     */
    public void setCourseList(List<SectionData> courseList) {
        this.courseList = courseList;
    }
}
