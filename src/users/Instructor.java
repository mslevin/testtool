package users;

import courses.Department;
import courses.SectionData;
import java.io.*;

import java.util.*;

/**
 * Created by Brandon on 10/19/2015.
 */
public class Instructor extends User implements Serializable {
    /**
     * The instructor's teaching department
     */
    public Department department;

    
    /**
     * A list of the courses taught by the given instructor
     */
    public List<SectionData> classList;
    
    public Instructor(String first, String last, Department department, String id, String password) {
        super(first, last, id, password);
        if (department == null) {
            throw new InputMismatchException();
        }
        this.department = department;
        classList = new ArrayList();
    }

    /**
     * Adds a new course to the list of courses taught by the given instructor
     * @param newCourse the new course to be added
     */
    /*
       pre: !exists(Course otherCourse;
          courseList.contains(otherCourse);
          course.department.equals(otherCourse.department)
          && course.courseNumber == otherCourse.courseNumber
      
       post: courseList'.size() == courseList.size() + 1
             && forall (Course containedCourse;
                        courseList.contains(containedCourse);
                        courseList'.contains(containedCourse))
             && courseList'.contains(containedCourse)
     */
    public void addCourse(SectionData newCourse) {
        classList.add(newCourse);
    }
    
    /**
     * Removes a course from the list of courses taught by the given instructor
     * @param course the course to be removed
     */
    /*
       pre: courseList.size() >= 1
            && courseList.contains(course)
      
       post: courseList'.size == courseList.size() - 1
             && !courseList'.contains(course)
             && forall (Course containedCourse;
                        courseList.contains(containedCourse) && containedCourse != newCourse;
                        courseList'.contains(containedCourse))
     */
    public void removeCourse(SectionData course) {
        classList.remove(course);
    }

    /**
     * @return the department
     */
    public Department getDepartment() {
        return department;
    }

    /**
     * @param department the department to set
     */
    public void setDepartment(Department department) {
        this.department = department;
    }

    /**
     * @return the classList
     */
    public ArrayList<SectionData> getClassList() {
        return (ArrayList)classList;
    }

    /**
     * @param classList the classList to set
     */
    public void setClassList(List<SectionData> classList) {
        this.classList = classList;
    }
}
