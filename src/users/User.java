package users;

import gui.Controller;
import java.io.*;
import java.util.InputMismatchException;

/**
 * Created by Brandon on 10/19/2015.
 */
public class User implements Serializable {
    /**
     * The first name of the given user
     */
    public String firstName;

    /**
     * The last name of the given user
     */
    public String lastName;

    /**
     * The user's Cal Poly Id
     */
    public String calPolyId;

    /**
     * The TestTool password associated with a given Cal Poly Id
     * !!!!!!SUPER SECURE PASSWORD!!!!!!!
     */
    public String password;

    public User(String first, String last, String id, String pass) {
        if (first == null || last == null || id == null || pass == null) {
            throw new InputMismatchException();
        }
        this.firstName = first;
        this.lastName = last;
        this.calPolyId = id;
        this.password = pass;
    }
    
    
    /**
     * This method is used to verify that the username and password provided are a valid combination
     * @param passwordAttempt The string supplied by the user in the password field
     * @return boolean value determining if the supplied input matched the stored password
     */
    /*
       pre: calPolyId != null
            && password != null
      
       post: return true
      
     */
    public boolean loginAttempt(String passwordAttempt) {
        if (passwordAttempt.equals(getPassword())) {
            return true;
        }
        return false;
    }
    
    public void logout() {
        Controller.currUser = null;
    }

    /**
     * @param firstName the firstName to set
     */
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    /**
     * @return the lastName
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * @param lastName the lastName to set
     */
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    /**
     * @return the calPolyId
     */
    public String getCalPolyId() {
        return calPolyId;
    }

    /**
     * @param calPolyId the calPolyId to set
     */
    public void setCalPolyId(String calPolyId) {
        this.calPolyId = calPolyId;
    }

    /**
     * @return the password
     */
    public String getPassword() {
        return password;
    }

    /**
     * @param password the password to set
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * @return the firstName
     */
    public String getFirstName() {
        return firstName;
    }
}
