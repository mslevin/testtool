
package gui;

import questions.*;
import tests.*;
import users.*;
import courses.*;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.*;

/**
 *
 * @author michael "the boss" slevin
 * Edited by Tim 12/3/15
 */
public class Controller {
    
    
    public static User currUser = null;
    public static SectionData currSection = null;
    public static Test currTest = null;
    
    public static InstructorBank iBank;
    public static StudentBank sBank;
    public static QuestionBank qBank;
    public static TestBank tBank;
    public static ClassBank cBank;
    public static MainFrame mf;
    
    // FOR TESTING ONLY
    private boolean ignoreSerial = false;
    
    public Controller() {
        iBank = new InstructorBank();
        sBank = new StudentBank();
        qBank = new QuestionBank();
        tBank = new TestBank();
        cBank = new ClassBank();
        
        // Try and read serialized objects
        if (!ignoreSerial) readSerialized();
        if (iBank.instructorList.isEmpty() || cBank.courseList.isEmpty()) {
            fillBanks();
        }
    }
    
    private static void fillBanks() {
        System.out.println("Initializing banks with data");
        
        // Add some classes to classBank
        cBank.addCourse(new SectionData(Department.CPE, 307, 01 , 2015, Quarter.FALL, "14-301", "MWF 12-2pm"));
        cBank.addCourse(new SectionData(Department.CPE, 448, 03 , 2015, Quarter.FALL, "14-301", "MWF 12-2pm"));
        cBank.addCourse(new SectionData(Department.CSC, 225, 07 , 2014, Quarter.SPRING, "14-301", "MWF 12-2pm"));
        cBank.addCourse(new SectionData(Department.SE, 308, 07 , 2014, Quarter.WINTER, "14-301", "MWF 12-2pm"));
        cBank.addCourse(new SectionData(Department.SE, 309, 07 , 2014, Quarter.SPRING, "14-301", "MWF 12-2pm"));

        // Add some instructors to instructorBank
        Instructor i = new Instructor("Michael", "Slevin", Department.CSC, "mslevin", "pass");
        // Add classes to instructor
        for (SectionData sd : cBank.courseList) {
            i.addCourse(sd);
        }
        
        
        // HAVE TO ADD COURSES TO INSTRUCTOR BEFORE ADDING TO INSTRUCTORBANK
        // Need to figure out way of fixing that later on
        
        iBank.addInstructor(i);
        iBank.addInstructor(new Instructor("John", "Doe", Department.CSC, "jdoe", "pass"));
        iBank.addInstructor(new Instructor("John", "Cena", Department.CSC, "champ", "pass"));
        iBank.addInstructor(new Instructor("Brad", "Pitt", Department.CPE, "bpitt", "pass"));
        iBank.addInstructor(new Instructor("Anna", "Kendrick", Department.SE, "akendrick", "pass"));
        
        // Add some students to studentBank
        sBank.addStudent(new Student("Danny", "Kennedy", "CSC", "dkennedy", "pass"));
        sBank.addStudent(new Student("Brandon", "Livitski", "CSC", "blivitsk", "pass"));
        sBank.addStudent(new Student("Tim", "Wirtz", "CSC", "twirtz", "pass"));
        sBank.addStudent(new Student("Gene", "Fisher", "SE", "gfisher", "pass"));
        sBank.addStudent(new Student("Eric", "Tran", "CPE", "etran", "pass"));
        sBank.addStudent(new Student("Joe", "Huang", "CPE", "rhuang", "pass"));
        sBank.addStudent(new Student("Bob", "Bobert", "CPE", "bob", "pass"));
        sBank.addStudent(new Student("Frank", "Frankfurt", "SE", "frank", "pass"));
        sBank.addStudent(new Student("Alex", "Anderson", "SE", "alex", "pass"));
        sBank.addStudent(new Student("Natalie", "Peters", "CSC", "natalie", "pass"));
        sBank.addStudent(new Student("Alanna", "Smith", "SE", "alanna", "pass"));
        sBank.addStudent(new Student("Cory", "Jones", "CSC", "cory", "pass"));
        
        //adding some questions to questionBank
        Map<String, Boolean> MCquestion1 = new HashMap();
        MCquestion1.put("a. boolean", true);
        MCquestion1.put("b. string", true);
        MCquestion1.put("c. array", true);
        MCquestion1.put("d. struct", false);
        MCquestion1.put("e. pointer", true);
        qBank.addQuestion(new MCQuestion("An array can contain which of the following types of data? (circle all that apply)",
            null, null, 5, "additional text", cBank.courseList.get(0), MCquestion1));
        
        Map<String, Boolean> MCquestion2 = new HashMap();
        MCquestion2.put("a. 1", false);
        MCquestion2.put("b. 2", false);
        MCquestion2.put("c. 3", false);
        MCquestion2.put("d. 4", true);
        MCquestion2.put("e. 5", false);
        qBank.addQuestion(new MCQuestion("How many byte(s) does an int have?",
            null, null, 5, "additional text", cBank.courseList.get(0), MCquestion2));
        
        Map<String, Boolean> MCquestion3 = new HashMap();
        MCquestion3.put("a. 2", false);
        MCquestion3.put("b. 4", false);
        MCquestion3.put("c. 6", false);
        MCquestion3.put("d. 8", true);
        MCquestion3.put("e. 10", false);
        qBank.addQuestion(new MCQuestion("How many byte(s) does a double have?",
            null, null, 5, "additional text", cBank.courseList.get(0), MCquestion3));
        
        Map<String, Boolean> MCquestion4 = new HashMap();
        MCquestion4.put("a. ALU", false);
        MCquestion4.put("b. Memory", false);
        MCquestion4.put("c. CPU", true);
        MCquestion4.put("d. Control unit", false);
        MCquestion4.put("e. None of the above", false);
        qBank.addQuestion(new MCQuestion("The brain of any computer system is",
            null, null, 5, "additional text", cBank.courseList.get(0), MCquestion4));
        
        Map<String, Boolean> MCquestion5 = new HashMap();
        MCquestion5.put("a. 01", false);
        MCquestion5.put("b. 110", false);
        MCquestion5.put("c. 11", true);
        MCquestion5.put("d. 10", false);
        MCquestion5.put("e. None of the above", false);
        qBank.addQuestion(new MCQuestion("Which of the following is the 1's complement of 10?",
            null, null, 5, "additional text", cBank.courseList.get(0), MCquestion5));
        
        //TrueFalseQuestion
        qBank.addQuestion(new TrueFalseQuestion("The operating system serves as an intermediary between the user and the computer hardware. (True /False)",
            null, null, 2, "additional text", cBank.courseList.get(1), true));
        
        qBank.addQuestion(new TrueFalseQuestion("The operating system serves as an intermediary between a process and the computer hardware. (True /False)",
            null, null, 2, "additional text", cBank.courseList.get(1), false));
        
        qBank.addQuestion(new TrueFalseQuestion("The control unit interprets program instructions and initiate control operations.",
            null, null, 2, "additional text", cBank.courseList.get(1), true));
        
        qBank.addQuestion(new TrueFalseQuestion("The binary system uses powers of 8.",
            null, null, 2, "additional text", cBank.courseList.get(1), false));
        
        qBank.addQuestion(new TrueFalseQuestion("A compiler converts assembly language to machine language.",
            null, null, 2, "additional text", cBank.courseList.get(1), false));
                
        //EssayQuestion
        qBank.addQuestion(new EssayQuestion("Write your definition of deep copy here.",
            null, null, 7, "additional text", cBank.courseList.get(2), "copy all elements of object"));
        
        qBank.addQuestion(new EssayQuestion("Write your definition of shallow copy here.",
            null, null, 7, "additional text", cBank.courseList.get(2), "copy of structure only not elements"));
        
        qBank.addQuestion(new EssayQuestion("What is a data structure?",
            null, null, 7, "additional text", cBank.courseList.get(2), "organizing data related efficient"));
        
        qBank.addQuestion(new EssayQuestion("What is the minimum nuber of queues needed to implement the priority queue?.",
            null, null, 7, "additional text", cBank.courseList.get(2), "two data priorities"));
        
        //MatchingQuestion
        Map<String, String> Matchingquestion1 = new HashMap();
        Matchingquestion1.put("a. code that performs task", "function");
        Matchingquestion1.put("b. objecting pointing to something", "pointer");
        Matchingquestion1.put("c. data type of true/false", "boolean");
        Matchingquestion1.put("d. data type of an integer", "int");
        Matchingquestion1.put("e. data type of larger float", "double");
        qBank.addQuestion(new MatchingQuestion("Match these terms.",
            null, null, 5, "additional text", cBank.courseList.get(3), Matchingquestion1));
        
        Map<String, String> Matchingquestion2 = new HashMap();
        Matchingquestion2.put("a. Char", "1");
        Matchingquestion2.put("b. Short", "2");
        Matchingquestion2.put("c. Int", "4");
        Matchingquestion2.put("d. Long", "8");
        Matchingquestion2.put("e. Double", "8");
        qBank.addQuestion(new MatchingQuestion("Match data types to size in bytes.",
            null, null, 5, "additional text", cBank.courseList.get(3), Matchingquestion2));
        
        //CodeQuestion
        qBank.addQuestion(new CodeQuestion("Write the code for a binary search.",
            null, null, 10, "additional text", cBank.courseList.get(4)));
        
        qBank.addQuestion(new CodeQuestion("Write the code for a depth first search.",
            null, null, 10, "additional text", cBank.courseList.get(4)));
        
        qBank.addQuestion(new CodeQuestion("Write the bode for a breadth first search.",
            null, null, 10, "additional text", cBank.courseList.get(4)));
        
        qBank.addQuestion(new CodeQuestion("Write the bode for a quick sort.",
            null, null, 10, "additional text", cBank.courseList.get(4)));
        
        SectionData c = cBank.courseList.get(0);
        for (Student s : sBank.studentList) {
            c.addStudent(s);
        }
        cBank.courseList.set(0, c);
    }
    
    public static boolean loginUser(String username, String password) {
        if (iBank.instructorList.size() > 0) { 
            for (Instructor instr : iBank.instructorList) {
                if (instr.getCalPolyId().equals(username.toLowerCase())) {
                    if (instr.getPassword().equals(password)) {
                        currUser = (User)instr;
                        System.out.println("Login Instructor: " + username);
                        return true;
                    }
                    return false;
                }
            }
        }
        if (sBank.studentList.size() > 0) {
            for (Student stu : sBank.studentList) {
                if (stu.getCalPolyId().equals(username.toLowerCase())) {
                    if (stu.getPassword().equals(password)) {
                        currUser = (User)stu;
                        System.out.println("Login Student: " + username);
                        return true;  
                    }
                    return false;
                }
            }
        } 
        return false; // Invalid username/password
    }
    
    /**
     * Adds a student to the student bank
     * @param s the student to be added
     */
    public static void addStudent(Student s) {
        sBank.addStudent(s);
    }
    
    /**
     * Gets a list of tests (active and history)
     * @return Array List of Test Options
     */
    public static ArrayList<TestOptions> getTests(String studentID) {
        ArrayList<TestOptions> sTests = new ArrayList<TestOptions>();
        Student student = findStudent(studentID);
        List<TestOptions> tests = tBank.getGivenTests();
        List<SectionData> list = student.getCourseList();
        if(list != null) {
            for(int i = 0; i < list.size(); i++) {
                for(int j = 0; i < tests.size(); j++) {
                    if(list.get(i).getCourseNumber() == tests.get(j).getCourse().getCourseNumber()) {
                        if(list.get(i).getDepartment() == (tests.get(j).getCourse().getDepartment())) {
                            sTests.add(tests.get(j));
                        }
                    }
                }
            }
        }
        return sTests;
    }
    
    public static List<AnswerSheet> getTests(SectionData sd) {
        List<AnswerSheet> list = new ArrayList();
        for (Student s : sd.enrollmentList) {
            for (AnswerSheet as : s.takenTestList) {
                if (as.test.section.equals(sd)) {
                    
                }
            }
        }
        return list;
    }
    
    /**
     * 
     * @param courseSection data for course section
     * @param options test options
     */
    public static void giveTest(SectionData courseSection, TestOptions options) {
        tBank.givenTests.add(options); 
        
    }
    
    /**
     * Finds a Student based on their id.
     * 
     * @param studentID ID of student being searched for
     * @return Student if there is one or null if not found
     */
    public static Student findStudent(String studentID) {
        Student student = null;
        List<Student> list = sBank.getStudentList();
        for(int i = 0; i < list.size(); i++) {
            if(list.get(i).calPolyId.compareTo(studentID) == 0) {
                student = list.get(i);
            }
        }  
        return student;
    }
    
    /**
     * Adds an instructor to the instructorBank
     * @param i the instructor to be added
     */
    public static void addInstructor(Instructor i) {
        iBank.addInstructor(i);
    }
    
    /**
     * Reads serialized objects from disk to load into the program
     */
    public static void readSerialized() {
        boolean windows = false;
        System.out.println("readSerialized: Working directory is " + System.getProperty("user.dir"));
        FileInputStream fileIn;
        if (System.getProperty("os.name").contains("Windows")) {
            System.out.println("Windows? Seriously? Come on.");
            windows = true;
        }
        try {
            if (windows) {
                fileIn = new FileInputStream(System.getProperty("user.dir") + "\\ClassBank.ser");
            }
            else {
                fileIn = new FileInputStream("/tmp/ClassBank.ser");
            }
            ObjectInputStream in = new ObjectInputStream(fileIn);
            cBank = (ClassBank) in.readObject();
            in.close();
            fileIn.close();
            
            if (windows) {
                fileIn = new FileInputStream(System.getProperty("user.dir") + "\\InstructorBank.ser");
            }
            else {
                fileIn = new FileInputStream("/tmp/InstructorBank.ser");
            }
            in = new ObjectInputStream(fileIn);
            iBank = (InstructorBank) in.readObject();
            in.close();
            fileIn.close();
            
            if (windows) {
                fileIn = new FileInputStream(System.getProperty("user.dir") + "\\StudentBank.ser");
            }
            else {
                fileIn = new FileInputStream("/tmp/StudentBank.ser");
            }
            in = new ObjectInputStream(fileIn);
            sBank = (StudentBank) in.readObject();
            in.close();
            fileIn.close();
            
            if (windows) {
                fileIn = new FileInputStream(System.getProperty("user.dir") + "\\QuestionBank.ser");
            }
            else {
                fileIn = new FileInputStream("/tmp/QuestionBank.ser");
            }
            in = new ObjectInputStream(fileIn);
            qBank = (QuestionBank) in.readObject();
            in.close();
            fileIn.close();
            
            if (windows) {
                fileIn = new FileInputStream(System.getProperty("user.dir") + "\\TestBank.ser");
            }
            else {
                fileIn = new FileInputStream("/tmp/TestBank.ser");
            }
            in = new ObjectInputStream(fileIn);
            tBank = (TestBank) in.readObject();
            in.close();
            fileIn.close();
        }
        catch (Exception e) { // Probably not the best way to do this lol
            System.out.println("Error: Serialized objects not read!");
            System.out.println(e);
            return;
        }
        
        System.out.println("Success: Serialized object read from disk");
    }
    
    
    /**
     * Writes all "bank" objects to disk on program close.
     * All "banks" implement Serializable. 
     */
    public static void serializeObjects() {
        boolean windows = false;
        System.out.println(System.getProperty("user.dir"));
        FileOutputStream fileOut;
        if (System.getProperty("os.name").contains("Windows")) {
            windows = true;
        }
        try { 
            if (windows) {
                fileOut = new FileOutputStream(System.getProperty("user.dir") + "\\ClassBank.ser");
            }
            else {
                fileOut = new FileOutputStream("/tmp/ClassBank.ser");
            }
            ObjectOutputStream out = new ObjectOutputStream(fileOut);
            out.writeObject(cBank);
            out.close();
            fileOut.close();
            
            if (windows) {
                fileOut = new FileOutputStream(System.getProperty("user.dir") + "\\InstructorBank.ser");
            }
            else {
                fileOut = new FileOutputStream("/tmp/InstructorBank.ser");
            }
            out = new ObjectOutputStream(fileOut);
            out.writeObject(iBank);
            out.close();
            fileOut.close();
            
            if (windows) {
                fileOut = new FileOutputStream(System.getProperty("user.dir") + "\\StudentBank.ser");
            }
            else {
                fileOut = new FileOutputStream("/tmp/StudentBank.ser");
            }
            out = new ObjectOutputStream(fileOut);
            out.writeObject(sBank);
            out.close();
            fileOut.close();
            
            if (windows) {
                fileOut = new FileOutputStream(System.getProperty("user.dir") + "\\QuestionBank.ser");
            }
            else {
                fileOut = new FileOutputStream("/tmp/QuestionBank.ser");
            }
            out = new ObjectOutputStream(fileOut);
            out.writeObject(qBank);
            out.close();
            fileOut.close();
            
            if (windows) {
                fileOut = new FileOutputStream(System.getProperty("user.dir") + "\\TestBank.ser");
            }
            else {
                fileOut = new FileOutputStream("/tmp/TestBank.ser");
            }
            out = new ObjectOutputStream(fileOut);
            out.writeObject(tBank);
            out.close();
            fileOut.close();

        } catch (Exception e) {
            System.out.println("Error: Objects not written to disk!");
            System.out.println(e);
            return;
        }
        System.out.println("Success: Banks written to disk.");
        
    }
    
    
    public static void addQuestionToTest(Question q) {
        currTest.questionList.add(q);
        System.out.println("adding question");
    }
    
}
