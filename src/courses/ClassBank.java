package courses;

import java.util.*;
import java.io.*;

/**
 * Created by Brandon on 10/20/2015.
 */
public class ClassBank implements Serializable {
    /**
     * A list of all courses that are registered in TestTool
     */
    public List<SectionData> courseList; 
    
    public ClassBank() {
        this.courseList = new ArrayList();
    }
    
    /**
     * This method adds a new course to the TestTool ClassBank
     * @param course a SectionData object representing the new course to be added 
     *</pre>
    
       pre: !exists(SectionData otherCourse; 
          courseList.contains(otherCourse);
          course.sectionNumber == otherCourse.sectionNumber
          && course.instructorId.equals(otherCourse.instructorId)
          && course.year == otherCourse.year
          && course.quarter.equals(otherCourse.quarter)
          && course.location.equals(otherCourse.location)
          && course.time.equals(otherCourse.time)
          && (forall (Student currentStudent;
                        course.enrollmentList.contains(currentStudent);
                        otherCourse.enrollmentList.contains(currentStudent)))
          && (forall (Student otherStudent;
                        otherCourse.enrollmentList.contains(otherStudent);
                        course.enrollmentList.contains(otherStudent))))
      
       post: courseList'.size() == courseList.size() + 1
             && forall (SectionData containedCourse;
                        courseList.contains(containedCourse);
                        courseList'.contains(containedCourse))
             && courseList'.contains(course)
     */
    public void addCourse(SectionData course) {
        courseList.add(course);
    }

    /**
     * This method removes the given course from the TestTool ClassBank
     * @param course a SectionData object representing the course to be removed
     */
    /*
       Pre: courseList.size() >= 1
            && courseList.contains(course)
      
       Post: courseList'.size == courseList.size() - 1
             && !courseList'.contains(course)
             && forall (SectionData containedCourse;
                        courseList.contains(containedCourse) && containedCourse != course;
                        courseList'.contains(containedCourse))
     */
    public void removeCourse(SectionData course) {
        // Needs to actually check that this course exists
        courseList.remove(course);
    }

    /**
     * @return the courseList
     */
    public List<SectionData> getCourseList() {
        return courseList;
    }

    /**
     * @param courseList the courseList to set
     */
    public void setCourseList(List<SectionData> courseList) {
        this.courseList = courseList;
    }
}
