package courses;

import java.io.*;

/**
 * Created by Brandon on 10/20/2015.
 */
public enum Quarter implements Serializable {
    FALL, WINTER, SPRING, SUMMER
}
