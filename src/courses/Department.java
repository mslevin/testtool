package courses;

import java.io.Serializable;

/**
 * Created by Brandon on 10/19/2015.
 */
public enum Department implements Serializable {
    CPE,CSC,SE
}
