package courses;

import users.Student;

import java.util.*;
import java.io.*;

/**
 * Created by Brandon on 10/19/2015.
 */
public class SectionData extends Course implements Serializable {
    /**
     * The section number of the given course
     */
    public int sectionNumber;

    /**
     * The Cal Poly Id of the course instructor
     */
    public String instructorId;
    
    /**
     * A list of all students enrolled in the given class
     */
    public List<Student> enrollmentList;
    
    /**
     * The year in which the given course was offered
     */
    public int year;
    
    /**
     * The quarter in which the given course was offered
     */
    public Quarter quarter;
    
    /**
     * The location where the given class was taught
     */
    public String location;
   
    /**
     * The time at which the given class started
     */
    public String time;

    public SectionData(Department dept, int courseNumber, int sectionNumber, 
            int year, Quarter quarter, String location, String time) {
        super(dept, courseNumber);
        this.sectionNumber = sectionNumber;
        this.year = year;
        this.quarter = quarter;
        this.location = location;
        this.time = time;
        
        enrollmentList = new ArrayList();
    }
    
    public boolean equals(SectionData s) {
        if (s.sectionNumber == this.sectionNumber &&
            s.year == this.year &&
            s.quarter == this.quarter &&
            s.location.equals(this.location) &&
            s.time == this.time) {
            return true;
        }
        return false;
    }

    /**
     * Enrolls a new student into the given course
     * @param student the student to be enrolled
     */
    public void addStudent(Student student) {
        getEnrollmentList().add(student);
    }

    /**
     * Drops a student from the given course
     * @param student the student to be dropped
     */
    public void dropStudent(Student student) {
        getEnrollmentList().remove(student);
    }

    /**
     * @return the sectionNumber
     */
    public int getSectionNumber() {
        return sectionNumber;
    }

    /**
     * @param sectionNumber the sectionNumber to set
     */
    public void setSectionNumber(int sectionNumber) {
        this.sectionNumber = sectionNumber;
    }

    /**
     * @return the instructorId
     */
    public String getInstructorId() {
        return instructorId;
    }

    /**
     * @param instructorId the instructorId to set
     */
    public void setInstructorId(String instructorId) {
        this.instructorId = instructorId;
    }

    /**
     * @return the enrollmentList
     */
    public List<Student> getEnrollmentList() {
        return enrollmentList;
    }

    /**
     * @param enrollmentList the enrollmentList to set
     */
    public void setEnrollmentList(List<Student> enrollmentList) {
        this.enrollmentList = enrollmentList;
    }

    /**
     * @return the year
     */
    public int getYear() {
        return year;
    }

    /**
     * @param year the year to set
     */
    public void setYear(int year) {
        this.year = year;
    }

    /**
     * @return the quarter
     */
    public Quarter getQuarter() {
        return quarter;
    }

    /**
     * @param quarter the quarter to set
     */
    public void setQuarter(Quarter quarter) {
        this.quarter = quarter;
    }

    /**
     * @return the location
     */
    public String getLocation() {
        return location;
    }

    /**
     * @param location the location to set
     */
    public void setLocation(String location) {
        this.location = location;
    }

    /**
     * @return the time
     */
    public String getTime() {
        return time;
    }

    /**
     * @param time the time to set
     */
    public void setTime(String time) {
        this.time = time;
    }
}
