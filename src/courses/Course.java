package courses;

import java.io.*;

/**
 * Created by Brandon on 10/19/2015.
 */
public class Course implements Serializable {
    /**
     * The department offering the given course
     */
    public Department department;

    /**
     * The designated course number of the given course
     */
    public int courseNumber;

    public Course(Department department, int courseNumber) {
        this.department = department;
        this.courseNumber = courseNumber;
    }
    
    public boolean equals(Course c) {
        return (this.department == c.department && this.courseNumber == c.courseNumber);
    }

    /**
     * @return the department
     */
    public Department getDepartment() {
        return department;
    }

    /**
     * @param department the department to set
     */
    public void setDepartment(Department department) {
        this.department = department;
    }

    /**
     * @return the courseNumber
     */
    public int getCourseNumber() {
        return courseNumber;
    }

    /**
     * @param courseNumber the courseNumber to set
     */
    public void setCourseNumber(int courseNumber) {
        this.courseNumber = courseNumber;
    }
}
